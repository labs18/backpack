#include <fstream>
#include <vector>

using namespace std;

ifstream fin("../input.txt");
ofstream fout("../output.txt");

struct item
{
	int weight, cost;
};

int w, n;
vector<item> items;
vector<vector<int>> a;

int backpack()
{
	a.resize(n + 1, vector<int>(w + 1, 0)); // ��� ������ ������������� � 0!

	for (size_t i = 1; i <= n; i++)
		for (size_t j = 1; j <= w; j++)
			a[i][j] = (j >= items[i].weight)
			? __max(a[i - 1][j], a[i - 1][j - items[i].weight] + items[i].cost)
			: a[i - 1][j];

	return a[n][w];
}

void findAns(int i, int j)
{
	if (!a[i][j]) return;

	if (a[i - 1][j] == a[i][j])
		findAns(i - 1, j);
	else
	{
		findAns(i - 1, j - items[i].weight);
		fout << i << ' ';
	}
}

int main()
{
	fin >> w >> n;
	items.resize(n + 1); // ������ ��������� � 1 �������������!

	for (size_t i = 1; i <= n; i++)
		fin >> items[i].weight >> items[i].cost;

	fin.close();

	fout << backpack() << endl;
	findAns(n, w);

	fout.close();
}